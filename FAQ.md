# Frequently Asked Questions (FAQ)

About Offcourse, an open source platform for crowdlearning [https://offcourse.io](https://offcourse.io)

## Contents
- Bugs
- Your account
- Courses
- User support
- Offcourse

## Bugs

### I’m trying to sign up for Offcourse, but my verification code isn’t accepted
This is a bug. Your verification code is probably accepted. Please try to sign in using the sign up button in the upper right corner of the screen. If this doesn’t work, please let us know [through this form](https://goo.gl/forms/eEL8ETML0qA22Pt03).

### I added content but it is not visible yet.
It takes about 1 minute for the platform to gather and display the content. Just wait and reload.

### I made a course, but I can’t find it on the platform
The homepage only shows a maximum of 20 courses. Click on your name in the upper right corner. You should see your course now. If this doesn’t work, please let us know [through this form](https://goo.gl/forms/eEL8ETML0qA22Pt03).

### I'm trying to create a fork, but the platform doesn't respond 
Please check if you used any special characters in your user name. If you did, the platform doesn't recognize your user name correctly and you can't fork. We can change your user name, send an email to [contact@offcourse.io](mailto:contact@offcourse.io).

## Your account

### I lost my password. How do I reset it/ create a new one?
Send an email to [contact@offcourse.io](mailto:contact@offcourse.io).

### How can I communicate with other users?
We are working on a tool for internal communication. We will announce any new features to the platform via [our monthly mailing](http://eepurl.com/clGDdz).

### How do I delete my account?
If you no longer want to use the platform you can simply log out. We will not share any of your personal details with third parties. If you want us to delete your account completely, send an email to [contact@offcourse.io](mailto:contact@offcourse.io).

## Courses

### How can I create my own/ edit other courses? 
To create a course sign in and click ‘Create Course’ in the top menu bar. To edit other courses click and go to a particular course and click ‘Edit’ on the bottom left corner of the card. Also, check our guidelines on [How to create a course on Offcourse](https://gitlab.com/offcourse/support/blob/master/How%20to%20create%20a%20course%20on%20Offcourse.md).

### Do I have to log in to view/ add/ edit the courses?
You can view courses without signing in. For editing courses or adding new courses sign in is required.

### How do I find topics I am interested in?
Every course has automated tags that explain what the course is about. By clicking on a tag you will get all courses about that topic.

### What is the idea behind the “checkpoints”?
Checkpoints are used for tasks. You can mark tasks when they are done.

### How can I complete tasks?
Click the checkpoint on the left side of the task to complete it.

### How can I share any of the course materials with someone else?
Use ‘Get URL’ at the bottom right of a card to copy the URL. You can now share it directly. Another way of sharing links is through Twitter and Facebook. Use the Twitter and Facebook icons at the bottom of the card.

## User support

### What if I find a “bug” on the platform?
We are in beta stage of development. We are happy to receive feedback you have on the platform that can help us improve. Please report any bugs on the platform [through this form](https://goo.gl/forms/eEL8ETML0qA22Pt03).

### I need help/ support, where do I go?
You can ask our support [through this form](https://goo.gl/forms/eEL8ETML0qA22Pt03).

### I have an idea/ request for a new feature, how can I tell you my ideas?
Send us your (feature) requests [through this form](https://goo.gl/forms/eEL8ETML0qA22Pt03).

## Offcourse

### What is the idea behind Offcourse? 
Offcourse is an open source platform that facilitates what we call crowdlearning. Crowdlearning to us is the believe that if everybody shares what they know in one place, everybody can learn what they want.

### What can can I do on the platform?

- Collect and save links to the web's best learning resources for your own learning
- Track progress of your learning
- Learn from others by tweaking a collection of links made by someone else and make it your own
- Create course materials so you can share what you know with others on the platform
- Share knowledge with others outside the platform through direct link sharing and social sharing



# Privacy Policy Offcourse 
## Last updated: May 22, 2018
### Website Visitors
Like most website operators, Offcourse collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Offcourse’s purpose in collecting non-personally identifying information is to better understand how Offcourse’s visitors use its website. From time to time, Offcourse may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.

### Gathering of Personally-Identifying Information
Certain visitors to Offcourse's websites choose to interact with Offcourse in ways that require Offcourse to gather personally-identifying information. The amount and type of information that Offcourse gathers depends on the nature of the interaction. For example, we may ask visitors who use our platform to provide a username and email address. In each case, Offcourse collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor's interaction with Offcourse. Offcourse does not disclose personally-identifying information other than as described below. And visitors can always refuse to supply personally-identifying information, with the caveat that it may prevent them from engaging in certain website-related activities.

### Aggregated Statistics
Offcourse may collect statistics about the behavior of visitors to its websites. For instance, Offcourse may reveal numbers on page views. However, Offcourse does not disclose personally-identifying information other than as described below.

### Protection of Certain Personally-Identifying Information
Offcourse discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors, and affiliated organizations that (i) need to know that information in order to process it on Offcourse's behalf or to provide services available at Offcourse's websites, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of your home country; by using Offcourse's websites, you consent to the transfer of such information to them. Offcourse will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors, and affiliated organizations, as described above, Offcourse discloses potentially personally-identifying and personally-identifying information only when required to do so by law, or when Offcourse believes in good faith that disclosure is reasonably necessary to protect the property or rights of Offcourse, third parties, or the public at large. If you are a registered user of an Offcourse website and have supplied your email address, Offcourse may occasionally send you an email to tell you about new features, solicit your feedback, or just keep you up to date with what's going on with Offcourse and our products. We primarily use our blog to communicate this type of information, so we expect to keep this type of email to a minimum. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. Offcourse takes all measures reasonably necessary to protect against the unauthorized access, use, alteration, or destruction of potentially personally-identifying and personally-identifying information.

### Cookies
A cookie is a string of information that a website stores on a visitor's computer, and that the visitor's browser provides to the website each time the visitor returns. Offcourse uses cookies to help Offcourse identify and track visitors, their usage of the Offcourse website, and their website access preferences. Offcourse visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using Offcourse's websites, with the drawback that certain features of Offcourse's websites may not function properly without the aid of cookies. The use of cookies by third parties is not covered by our privacy policy. 

### Security
Offcourse takes reasonable steps to protect the personally-identifying information provided via the website from loss, misuse, and unauthorized access, disclosure, alteration, or destruction. However, no Internet or email transmission is ever fully secure or error free. Therefore, you should take special care in deciding what information you send to us via email. Please keep this in mind when disclosing any Personal Data to Offcourse via the Internet.

### Privacy Policy Changes
Although most changes are likely to be minor, Offcourse may change its Privacy Policy from time to time, and in Offcourse's sole discretion. Offcourse encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.

### Access to Information; Contacting Us
To keep your Personal Data accurate, current, and complete, please contact us as specified below. We will take reasonable steps to update or correct personally-identifying information in our possession that you have previously submitted via our website.

Please also feel free to contact us if you have any questions about Offcourse's Privacy Policy or the information practices of the website.

You may contact us at contact@offcourse.io

Creative Commons Sharealike license, parts taken from ![WordPress.com](https://wordpress.com/) Privacy Policy
